import config from "../config";/**
 * Load the cars from the spreadsheet
 * Get the right values from it and assign.
 */
export function load(callback) {
  window.gapi.client.load("sheets", "v4", () => {
    window.gapi.client.sheets.spreadsheets.values
      .get({
        spreadsheetId: config.spreadsheetId,
        range: "Sovereign Wealth Funds!A2:T"
      })
      .then(
        response => {
          const data = response.result.values;  
          console.log("hjg", data);        
          const invData = data.map(audience => ({
            handle: audience[0],
            followers: audience[1],
            // model: car[2]
          })) || [];          
          callback({
            invData
          });
        },
        response => {
          callback(false, response.result.error);
        }
      );
  });
}
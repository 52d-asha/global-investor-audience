import React, { Component } from 'react';
import { load } from '../helpers/spreadsheet';
import config from '../config';
import {
  useTable,
  useGroupBy,
  useFilters,
  useSortBy,
  useExpanded,
  usePagination,
} from 'react-table'
// import TableFilter from 'react-table-filter';
// import react-table-filter/lib/styles.css;

class DataList extends Component {


    state = {
        invData: [],
        error: null
    }

    componentDidMount() {
        // 1. Load the JavaScript client library.
        window.gapi.load("client", this.initClient);
     }

     onLoad = (data, error) => {
        console.log('data', data);
        console.log('error',error);
        
        if (data) {
          const invData = data.invData;
          this.setState({ invData });
        } else {
          this.setState({ error });
        }
      };

     initClient = () => {
        // 2. Initialize the JavaScript client library.
        window.gapi.client
          .init({
            apiKey: config.apiKey,
            // Your API key will be automatically added to the Discovery Document URLs.
            discoveryDocs: config.discoveryDocs
          })
          .then(() => {
          // 3. Initialize and make the API request.
          load(this.onLoad);
          
        });

      
      };

      render() {
        const { invData, error } = this.state;
          if (error) {
            return <div>{this.state.error}</div>;
        }
        return (
          <div>
            <p>This will be the data list</p>
            <table>
              <tbody>
                <tr>
                  <th>Handle</th>
                  <th>Followers</th>
                </tr>
                {invData.map((audience, i) => (
                <tr>                 
                    
                    <td key={i}>
                        {audience.handle}
                    </td>
                    <td key={i}>
                        {audience.followers}
                    </td>
                  
                </tr>
                ))}
              </tbody>
            </table>          
          </div>
        
        );
  
        
      }

  }
  export default DataList;
